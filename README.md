![alt text](https://gitlab.com/hp3icc/Easy-DVSwitch/-/raw/main/dvs-emq.jpg?inline=false)

# Easy-DVSwitch

Script allows to perform blank installations of DVSwitch server application, in case user deletes folders or configuration files, you can run this script, to re-install DVSwitch server without formatting the operating system

This script applies solutions from Steve N4IRS, to fix error when reinstalling complete DVSwitch Server.

# Ports

http = 80 TCP

websocket = 9009 TCP


# Dashboard

Dashboard & EasyUserInterface mod by DS5QDR

# Install

     apt-get update
 
     apt-get install curl sudo -y
 
     bash -c "$(curl -fsSL https://gitlab.com/hp3icc/Easy-DVSwitch/-/raw/main/install.sh)"
#

## credits


Special thanks to Steve N4IRS, Claudio LU8ANB, Shane M0VUB, Francois F4JBM, for their guides in the allstar installation process

# Source

Dvswitch oficial group : https://dvswitch.groups.io/g/main?

Fix Reinstall by N4IRS : https://dvswitch.groups.io/g/main/topic/86568671

Dashboard mod by DS5QDR : https://ds5qdr-dv.tistory.com/m/450
#
